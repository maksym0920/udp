﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Client {
    class Program {
        public static void Authorization(Dictionary<string, object> Request) {
            int result = (int) Request["result"];
            string userID = Request["userId"] as string;

            Client.UserId = userID;
            Console.WriteLine("Authorization DONE!");
        }

        public static void ChangeName(Dictionary<string, object> Request) {
            int result = (int) Request["result"];
            Console.WriteLine("ChangeName DONE!");
        }
        
        public static void AddDevice(Dictionary<string, object> Request) {
            int result = (int) Request["result"];
            Console.WriteLine("AddDevice DONE!");
        }
        
        public static void Data(Dictionary<string, object> Request) {
    
            Console.WriteLine("Data DONE!");
        }


        public static void Main(string[] args) {
            Client.Init("dd56cda8929d3f698c6b819cea31fb80796cadfeda22a1a87191847de790f97d", 1);
            
            Client.AddHandler("user:authorization", Authorization);
            Client.AddHandler("user:changeName", ChangeName);
            Client.AddHandler("user:addDevice", AddDevice);
            Client.AddHandler("data:receive", Data);
            
            var data = new Hashtable();
            data["deviceId"] = new[] {"NEW DEVICE ID","asd33","asd44"};
            Client.Send("user:authorization", data);
            Thread.Sleep(1000); // ждем пока не получим userID
            
            var data2 = new Hashtable();
            data2["newName"] = "NEW NAME 123";
            Client.Send("user:changeName", data2);
            
            var data3 = new Hashtable();
            data3["deviceId"] = "NEW DEVICE ID";
            Client.Send("user:addDevice", data3);
            
            var data4 = new Hashtable();
            data4["data"] = new string[]{"user"};
            Client.Send("data:receive", data4);
            
            
            Console.ReadLine();
            Console.WriteLine("Global END!");
        }
    }
}