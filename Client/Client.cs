﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Client.Lib;

namespace Client {
    public static class Client {
        private static UdpClient _client;
        private static IPEndPoint _endPoint;
        private static byte[] _secret;
        private static Dictionary<string, Action<Dictionary<string, object>>> _handlers;

        public static string UserId { get; set; }
        public static int Version { get; set; }

        public static void Init(string userId = "", int version = 1) {
            _client = new UdpClient();
            _endPoint = new IPEndPoint(IPAddress.Parse("93.126.95.232"), 9000);
            _client.Connect(_endPoint);
            UserId = userId;
            _handlers = new Dictionary<string, Action<Dictionary<string, object>>>();
            _secret = new byte[] {115, 114, 73, 12, 13, 15, 77, 88, 99, 1, 188, 12, 244, 137, 66, 7};
            Version = version;

            _client.BeginReceive(new AsyncCallback(ReceiveCallback), _client);
        }

        public static void AddHandler(string router, Action<Dictionary<string, object>> process) {
            _handlers.Add(router, process);
        }

        public static void Send(string route, object data) {
            Protocol shell = new Protocol();
            shell.UserId = UserId;
            shell.Version = Version;
            shell.Route = route;
            shell.Data = data;
            shell.Error = "";

            var bytes = Lib.Convert.ToByteArray(shell, _secret);

            _client.Send(bytes, bytes.Length); // отправка
        }

        private static void ReceiveCallback(IAsyncResult ar) {
            byte[] receiveBytes = _client.EndReceive(ar, ref _endPoint);
            Protocol shell = Lib.Convert.FromByteArray(receiveBytes, _secret);
            Show.Dictionary(shell.Data as Dictionary<string, object>);
            
            var request = shell.Data as Dictionary<string, object>;
            if (request.ContainsKey("error")) {
                Console.WriteLine(request["error"]);
                return;
            }
            
            if (_handlers.ContainsKey(shell.Route)) {
                _handlers[shell.Route](request);
            }

            _client.BeginReceive(new AsyncCallback(ReceiveCallback), _client);
        }
    }
}