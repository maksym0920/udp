﻿﻿using System;
using System.Collections;

namespace Client.Lib {
    public static class Show {
        public static void List(IEnumerable obj, string prefix = "") {
            foreach (var val in obj) {
                if (val is string) {
                    Console.WriteLine(prefix + val);
                } else if (val is IEnumerable) {
                    List(val as IEnumerable, prefix + "--->");
                } else {
                    Console.WriteLine(prefix + val);
                }
            }
        }

        public static void Dictionary(IDictionary obj, string prefix = "") {
            foreach (var val in obj.Keys) {
                if (obj[val] is IDictionary) {
                    Console.WriteLine(prefix + val + ": ");
                    Dictionary((IDictionary) obj[val], prefix + "--->");
                } else if (obj[val] is string) {
                    Console.WriteLine(prefix + "[" + val + " : " + obj[val] + "]");
                } else if (obj[val] is IEnumerable) {
                    Console.WriteLine(prefix + val + ": ");
                    List((IEnumerable) obj[val], prefix + "--->");
                } else {
                    Console.WriteLine(prefix + "[" + val + " : " + obj[val] + "]");
                }
            }
        }
    }
}