﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;

namespace Client.Lib {
    public struct Protocol {
        public string UserId;
        public int Version;
        public string Route;
        public string Error;
        public object Data;
    }

    /// <summary>
    /// Сериализация и десериализация
    /// Protocol в массив байт и обратно
    /// </summary>
    public static class Convert {
        private static object _toObject(BinaryReader reader, ref bool isEnd) {
            object ans = null;
            char type = reader.ReadChar();
            switch (type) {
                case 'E':
                    isEnd = true;
                    break;
                case 'H':
                    var ht = new Dictionary<string, object>();
                    bool isOverH = false;
                    while (true) {
                        var key = _toObject(reader, ref isOverH);
                        if (isOverH) {
                            break;
                        }

                        var value = _toObject(reader, ref isOverH);
                        ht.Add(key.ToString(), value);
                    }

                    ans = ht;
                    break;
                case 'A':
                    ArrayList al = new ArrayList();
                    bool isOverA = false;
                    while (true) {
                        var tmp = _toObject(reader, ref isOverA);
                        if (isOverA) {
                            break;
                        }

                        al.Add(tmp);
                    }

                    ans = al;
                    break;

                case 'N':
                    break;
                case 'S':
                    ans = reader.ReadString();
                    break;

                case 'a':
                    ans = reader.ReadBoolean();
                    break;
                case 'b':
                    ans = reader.ReadByte();
                    break;
                case 'c':
                    ans = reader.ReadChar();
                    break;
                case 'd':
                    ans = reader.ReadDecimal();
                    break;
                case 'e':
                    ans = reader.ReadDouble();
                    break;
                case 'f':
                    ans = reader.ReadInt16();
                    break;
                case 'g':
                    ans = reader.ReadInt32();
                    break;
                case 'h':
                    ans = reader.ReadInt64();
                    break;
                case 'i':
                    ans = reader.ReadSByte();
                    break;
                case 'j':
                    ans = reader.ReadUInt32();
                    break;
                case 'k':
                    ans = reader.ReadUInt64();
                    break;
                case 'l':
                    ans = reader.ReadUInt16();
                    break;
                case 'm':
                    ans = DateTime.FromBinary(reader.ReadInt64());
                    break;
            }

            return ans;
        }

        private static void _toByteArray(BinaryWriter writer, object obj) {
            //bool a
            //byte b
            //char c
            //decimal d
            //double //float e 
            //short f
            //int g
            //long h
            //sbyte i
            //uint j
            //ulong k
            //ushort l
            //DateTime m

            //string S
            //null N

            // IDictionary
            // Hashtable // Dictionary<key, value> H
            // IEnumerable
            // ArrayList //List<value> //array[] A

            //object (K,V)

            if (obj is null) {
                writer.Write('N');
            } else if (obj is string) {
                writer.Write('S');
                writer.Write(obj as string);
            } else if (obj is ValueType) {
                if (obj is bool) {
                    writer.Write('a');
                    writer.Write((bool) obj);
                } else if (obj is byte) {
                    writer.Write('b');
                    writer.Write((byte) obj);
                } else if (obj is char) {
                    writer.Write('c');
                    writer.Write((char) obj);
                } else if (obj is decimal) {
                    writer.Write('d');
                    writer.Write((decimal) obj);
                } else if (obj is double || obj is float) {
                    writer.Write('e');
                    writer.Write((double) obj);
                } else if (obj is short) {
                    writer.Write('f');
                    writer.Write((short) obj);
                } else if (obj is int) {
                    writer.Write('g');
                    writer.Write((int) obj);
                } else if (obj is long) {
                    writer.Write('h');
                    writer.Write((long) obj);
                } else if (obj is sbyte) {
                    writer.Write('i');
                    writer.Write((sbyte) obj);
                } else if (obj is uint) {
                    writer.Write('j');
                    writer.Write((uint) obj);
                } else if (obj is ulong) {
                    writer.Write('k');
                    writer.Write((ulong) obj);
                } else if (obj is ushort) {
                    writer.Write('l');
                    writer.Write((ushort) obj);
                } else if (obj is DateTime) {
                    writer.Write('m');
                    writer.Write(((DateTime) obj).ToBinary());
                } else {
                    throw new Exception("Unknown type");
                }
            } else if (obj is IDictionary) {
                IDictionary cur = obj as IDictionary;
                writer.Write('H');
                foreach (var entry in cur.Keys) {
                    writer.Write('S');
                    writer.Write(entry.ToString());
                    _toByteArray(writer, cur[entry]);
                }

                writer.Write('E');
            } else if (obj is IEnumerable) {
                IEnumerable cur = obj as IEnumerable;
                writer.Write('A');
                foreach (var val in cur) {
                    _toByteArray(writer, val);
                }

                writer.Write('E');
            } else {
                writer.Write('H');
                foreach (PropertyInfo pInfo in obj.GetType().GetProperties()) {
                    writer.Write('S');
                    writer.Write(pInfo.Name);
                    _toByteArray(writer, pInfo.GetValue(obj, null));
                }

                foreach (FieldInfo fInfo in obj.GetType().GetFields()) {
                    writer.Write('S');
                    writer.Write(fInfo.Name);
                    _toByteArray(writer, fInfo.GetValue(obj));
                }

                writer.Write('E');
            }
        }

        /// <summary>
        /// Сериализация объекта в массив байт, с защитой от изменения запроса
        /// </summary>
        /// <param name="response">Объект для сеарилизации</param>
        /// <param name="secret">16 byte секретный ключ</param>
        public static byte[] ToByteArray(Protocol response, byte[] secret) {
            var stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream)) {
                using (MD5 md5Hash = MD5.Create()) {
                    // write object
                    writer.Write(response.UserId);
                    writer.Write(response.Version);
                    writer.Write(response.Route);
                    writer.Write(response.Error);
                    _toByteArray(writer, response.Data);

                    // write secret
                    writer.Write(secret);

                    // get request hash
                    byte[] hash = md5Hash.ComputeHash(stream.ToArray());

                    // replace secret to hash
                    var res = stream.ToArray();
                    Replace16Byte(res, hash);

                    return res;
                }
            }
        }

        /// <summary>
        /// Десериализация, с проверкой от изменения запроса
        /// </summary>
        /// <param name="bytes">Массив байт для десериализации</param>
        /// <param name="secret">16 byte секретный ключ</param>
        /// <returns>Десериализированный объект</returns>
        public static Protocol FromByteArray(byte[] bytes, byte[] secret) {
            MemoryStream stream = new MemoryStream(bytes);
            using (BinaryReader reader = new BinaryReader(stream)) {
                using (MD5 md5Hash = MD5.Create()) {
                    // read object
                    Protocol obj = new Protocol();
                    obj.UserId = reader.ReadString();
                    obj.Version = reader.ReadInt32();
                    obj.Route = reader.ReadString();
                    obj.Error = reader.ReadString();
                    bool isEnd = false;
                    obj.Data = _toObject(reader, ref isEnd) as Dictionary<string, object>;

                    // read receive hash
                    byte[] receiveHash = reader.ReadBytes(16);

                    // replace hash to secret
                    var receiveBytes = stream.ToArray();
                    Replace16Byte(receiveBytes, secret);

                    // get real hash
                    byte[] realHash = md5Hash.ComputeHash(receiveBytes);

                    // request validation
                    if (!receiveHash.SequenceEqual(realHash)) {
                        throw new Exception("Wrong Hash!");
                    }

                    return obj;
                }
            }
        }

        private static void Replace16Byte(byte[] main, byte[] newHash) {
            for (int i = main.Length - 16, j = 0; i < main.Length; i++, j++) {
                main[i] = newHash[j];
            }
        }
    }
}